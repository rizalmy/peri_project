create table tbl_Customer(
CustomerID int identity primary key,
UserName varchar(50),
Email varchar(50),
Password varchar(MAX),
IsActive bit,
Name varchar(100),
Phone varchar(15),
NoKtp varchar(16),
Address varchar(300),
ActivationCode uniqueidentifier
)

insert into tbl_Customer(UserName, Email, Password, IsActive)
values ('default', 'default', 'default', 1)

create table tbl_RoleUser(
RoleID int identity primary key,
Role_User varchar(20),
)

insert into tbl_RoleUser
(Role_User)
values
('Admin'),('Owner')

create table tbl_Admin(
AdminID int identity primary key,
UserName varchar(50),
Password varchar(50),
fk_RoleID int foreign key references tbl_RoleUser(RoleID)
)

insert into tbl_Admin
(UserName, Password, fk_RoleID)
values
('Owner', 'logowner123', 2),
('Admin', 'logAdmin987', 1)

create table tbl_design
(
pk_id_design int identity primary key,
difficulty varchar(50),
design_price float,
)

create table tbl_product
(
pk_id_product int identity primary key,
product_name varchar(50),
product_price float
)

create table tbl_spending
(
pk_id_spending int identity primary key,
spending_name varchar(250),
spending_cash float,
payment float,
date date
)

create table tbl_customerOrder(
pk_id_customerOrder int identity primary key,
fk_id_customer int,
name varchar(50),
phone varchar(50),
email varchar(50),
constraint fk_customer foreign key(fk_id_customer)
references tbl_Customer(CustomerID)
)

create table tbl_order(
pk_id_order int identity primary key,
fk_customer_id int,
details_order varchar(600),
lenght_meter float,
width_meter float,
fk_design_id int,
fk_product_id int,
amount float,
price float,
date date,
order_status varchar(20),
constraint fk_designorder foreign key (fk_design_id)
references tbl_design(pk_id_design),
constraint fk_productOrder foreign key (fk_product_id)
references tbl_product (pk_id_product),
constraint fk_customerOrder_order foreign key (fk_customer_id)
references tbl_customerOrder(pk_id_customerOrder)
)

create table tbl_payment (
pk_id_payment int identity primary key,
fk_id_customerOrder int,
total_price float,
payment float,
payment_status varchar(20),
order_status varchar(20),
date date,
constraint fk_customerOrder_payment foreign key (fk_id_customerOrder)
references tbl_customerOrder(pk_id_customerOrder)
)

create table tbl_customerDebt(
pk_id_cDebt int identity primary key,
fk_id_payment int, 
Debt float,
constraint fkpaymentdebt foreign key(fk_id_payment)
references tbl_payment(pk_id_payment)
)

alter table tbl_customerDebt
add debt_status varchar (20)

alter table tbl_customerDebt
add payment float


create table tbl_Owner_debt(
pk_id_oDebt int identity primary key,
fk_id_spending int,
debt int,
constraint fkOdebtSpending foreign key(fk_id_spending)
references tbl_spending (pk_id_Spending)
)

create table tbl_otherIncome (
pk_id_otherIncome int identity primary key,
incomeName varchar(50),
income float,
date date
)


--Stored procedure for get total profit
CREATE PROCEDURE spGetProfitTotal(@paramYear as int)
AS
BEGIN
    select sum(coalesce(tbl_payment.payment,0)+coalesce(income,0)-coalesce(tbl_spending.payment,0))as profit_year
    from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date 
    left join tbl_spending on tbl_payment.date=tbl_spending.date where year(tbl_payment.date)=@paramYear
END

--stored procedure for get total income
CREATE PROCEDURE spGetIncomeTotal(@paramYear as int)
AS
BEGIN
     select sum(coalesce(tbl_payment.payment,0)+coalesce(income,0))as income_year
     from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date 
     where year(tbl_payment.date)=@paramYear
END

--stored procedure for get total spending
CREATE PROCEDURE spGetSpendingTotal(@paramYear as int)
AS
BEGIN
	select sum(coalesce(tbl_spending.payment,0))as Spending_Year
    from  tbl_spending where year(tbl_spending.date)=@paramYear
END

------fix stored procedure----------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[spGetIncomeTotal](@paramYear as int)
AS
BEGIN
     select coalesce(sum(coalesce(tbl_payment.payment,0)+coalesce(income,0)),0)as income_year
     from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date 
     where year(tbl_payment.date)=@paramYear
END

ALTER PROCEDURE [dbo].[spGetProfitTotal](@paramYear as int)
AS
BEGIN
    select coalesce(sum(coalesce(tbl_payment.payment,0)+coalesce(income,0)-coalesce(tbl_spending.payment,0)),0)as profit_year
    from tbl_payment left join tbl_otherIncome on tbl_payment.date=tbl_otherIncome.date 
    left join tbl_spending on tbl_payment.date=tbl_spending.date where year(tbl_payment.date)=@paramYear
END


ALTER PROCEDURE [dbo].[spGetSpendingTotal](@paramYear as int)
AS
BEGIN
	select coalesce(sum(coalesce(tbl_spending.payment,0)),0)as Spending_Year
    from  tbl_spending where year(tbl_spending.date)=@paramYear
END

-------------------------------07/Mei/2020----------------------------------------------------
alter table tbl_product
add ImagePath varchar(max)

alter table tbl_payment
add ImagePath varchar(max)

alter table tbl_order
add ImagePath varchar(max)

alter table tbl_Customer
add ImagePath varchar(max)

select*from tbl_Customer

select * from tbl_product

--profit
select sum(tbl_payment.payment)

--income
select sum(coalesce(tbl_otherIncome.income,0)), date from tbl_otherIncome group by date
select sum(coalesce(tbl_payment.payment,0)) as Income, date from tbl_payment group by date having MONTH(date)=4

--spending
select sum
