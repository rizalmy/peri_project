﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PERI_Project.Models
{
    [MetadataType(typeof(CustomerMetaData))]
    public partial class tbl_Customer
    {
        public string ConfirmPassword { get; set; }

    }

    public class CustomerMetaData
    {
        [Display(Name = "User Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name required")]
        public string UserName { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Full Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Full Name required")]
        public string Name { get; set; }

        [Display(Name = "Phone Number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone Number required")]
        [MaxLength(13, ErrorMessage = "Input number, more than 13 number")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "KTP Number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "KTP Number required")]
        [MinLength(16, ErrorMessage = "Input number, less than 16 number")]
        [MaxLength(16, ErrorMessage = "Input number, more than 16 number")]
        public string NoKtp { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "KTP File required")]
        [Display(Name = "Upload KTP")]
        public string ImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address required")]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password required")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Minimum 6 character required")]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirm password and password do not match")]
        public string ConfirmPassword { get; set; }

    }
}