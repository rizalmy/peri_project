//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PERI_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_customerDebt
    {
        public int pk_id_cDebt { get; set; }
        public Nullable<int> fk_id_payment { get; set; }
        public Nullable<double> Debt { get; set; }
        public string debt_status { get; set; }
        public Nullable<double> payment { get; set; }
    
        public virtual tbl_payment tbl_payment { get; set; }
    }
}
