﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models; // add models

namespace PERI_Project.Controllers
{
    public class AdminLoginController : Controller
    {
        // GET: AdminLogin
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Autherize(PERI_Project.Models.tbl_Admin umodel)
        {
            using (PERIEntityDB db = new PERIEntityDB()) //new object from DB project
            {
                var userDetails = db.tbl_Admin.Where(x => x.UserName == umodel.UserName && x.Password == umodel.Password).FirstOrDefault();
                if (userDetails == null)
                {
                    umodel.LoginErrorMessage = "Username or Password Wrong !";
                    return View("Login", umodel);
                }
                else
                {
                    Session["userID"] = userDetails.AdminID;
                    Session["userName"] = userDetails.UserName;

                    if (userDetails.fk_RoleID == 1)
                    {
                        return RedirectToAction("index", "payment");  // url to admin
                    }
                    else if (userDetails.fk_RoleID == 2)
                    {
                        return RedirectToAction("Index", "Home"); // url to owner
                    }
                    else
                    {
                        return RedirectToAction("Login", "AdminLogin");
                    }
                }
            }
        }
        public ActionResult LogOut()
        {
            int userId = (int)Session["userID"];
            Session.Abandon();
            return RedirectToAction("Login", "AdminLogin");
        }
    }
}