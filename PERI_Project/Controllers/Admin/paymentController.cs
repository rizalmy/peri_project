﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Admin
{
    public class paymentController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: payment
        public ActionResult Index()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder);
            return View(tbl_payment.ToList());
        }

        public ActionResult Income()
        {
            var tbl_payment = db.tbl_payment.Include(t => t.tbl_customerOrder);
            return View(tbl_payment.ToList());
        }

        // GET: payment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            var order = db.tbl_order.Where(item => item.fk_customer_id == tbl_payment.fk_id_customerOrder);
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: payment/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name");
            return View();
        }

        // POST: payment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_payment,fk_id_customerOrder,total_price,payment,payment_status,order_status,date")] tbl_payment tbl_payment)
        {

            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                { try
                    {
                        db.tbl_payment.Add(tbl_payment);
                        db.SaveChanges();
                        transaction.Commit(); 
                        return RedirectToAction("Index");
                    } 
                    catch (Exception ex) 
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    } 
                }     
                
            }

            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_payment.fk_id_customerOrder);
            return View(tbl_payment);
        }

        // GET: payment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_customerOrder = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_payment.fk_id_customerOrder);
            return View(tbl_payment);
        }

        // POST: payment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit([Bind(Include = "pk_id_payment,fk_id_customerOrder,total_price,payment,payment_status,order_status,date")] tbl_payment order_total)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        order_total.payment += order_total.payment;
                        db.Entry(order_total).State = EntityState.Modified;
                        tbl_customerDebt debt = db.tbl_customerDebt.Where(item => item.fk_id_payment == order_total.pk_id_payment).FirstOrDefault();
                        if (debt == null)
                        {
                            if (order_total.payment > order_total.total_price)
                            {
                                order_total.payment = order_total.total_price;
                            }
                            if (order_total.payment == order_total.total_price)
                            {
                                order_total.payment_status = "Done";
                                //tbl_customer_debt debt = db.tbl_customer_debt.Where(item => item.fk_id_payment == order_total.pk_id_OrderTotal).FirstOrDefault();

                            }
                            else
                            {
                                order_total.payment_status = "Debt";
                                tbl_customerDebt debt2 = new tbl_customerDebt();
                                debt2.fk_id_payment = order_total.pk_id_payment;
                                debt2.Debt = order_total.total_price - order_total.payment;
                                debt2.debt_status = "Not Yet";
                                db.tbl_customerDebt.Add(debt2);
                                db.SaveChanges();
                                transaction.Commit();
                            }
                        }
                        else
                        {
                            if (order_total.payment > order_total.total_price)
                            {
                                order_total.payment = order_total.total_price;
                            }
                            if (order_total.payment == order_total.total_price)
                            {
                                order_total.payment_status = "Done";
                                //tbl_customer_debt debt = db.tbl_customer_debt.Where(item => item.fk_id_payment == order_total.pk_id_OrderTotal).FirstOrDefault();
                                debt.payment = debt.Debt;
                                if (debt.payment == debt.Debt)
                                {
                                    debt.debt_status = "Done";
                                    db.Entry(debt).State = EntityState.Modified;
                                }
                            }
                            else
                            {
                                order_total.payment_status = "Debt";
                                //tbl_customer_debt debt = new tbl_customer_debt();
                                //debt.fk_id_payment = order_total.pk_id_OrderTotal;
                                debt.Debt = order_total.total_price - order_total.payment;
                                debt.debt_status = "Not Yet";
                                db.Entry(debt).State = EntityState.Modified;
                                db.SaveChanges();
                                transaction.Commit();
                            }
                        }
                        //order_total.date = DateTime.Today;

                        db.Entry(order_total).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }
            }
 
 
            //ViewBag.fk_id_customer = new SelectList(db.customers, "pk_id_customer", "name", order_total.fk_id_customer);
            return View(order_total);
        }

        // GET: payment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_payment tbl_payment = db.tbl_payment.Find(id);
            if (tbl_payment == null)
            {
                return HttpNotFound();
            }
            return View(tbl_payment);
        }

        // POST: payment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            { try
                {
                    tbl_payment tbl_payment = db.tbl_payment.Find(id);
                    db.tbl_payment.Remove(tbl_payment);
                    db.SaveChanges();
                    transaction.Commit();
                } 
                catch (Exception ex) 
                {
                    transaction.Rollback(); ViewData["Data"] = ex.Message;
                }
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
