﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Admin
{
    public class customerOrderController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: customerOrder
        public ActionResult Index()
        {
            var tbl_customerOrder = db.tbl_customerOrder.Include(t => t.tbl_Customer);
            return View(tbl_customerOrder.ToList());
        }

        // GET: customerOrder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerOrder tbl_customerOrder = db.tbl_customerOrder.Find(id);
            if (tbl_customerOrder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_customerOrder);
        }

        // GET: customerOrder/Create
        public ActionResult Create()
        {
            tbl_Customer Customer = db.tbl_Customer.Find(1);
            tbl_customerOrder order = new tbl_customerOrder();
            order.fk_id_customer = Customer.CustomerID;
            //ViewBag.fk_id_customer = new SelectList(db.tbl_Customer, "CustomerID", "UserName");
            return View(order);
        }

        // POST: customerOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_customerOrder,fk_id_customer,name,phone,email")] tbl_customerOrder tbl_customerOrder)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                { try
                    {
                        Session["Customer"] = tbl_customerOrder;
                        db.tbl_customerOrder.Add(tbl_customerOrder);
                        db.SaveChanges(); 
                        transaction.Commit(); 
                        return RedirectToAction("Create", "order");
                    } 
                    catch (Exception ex) 
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }

            }

            //ViewBag.fk_id_customer = new SelectList(db.tbl_Customer, "CustomerID", "UserName", tbl_customerOrder.fk_id_customer);
            return View(tbl_customerOrder);
        }

        // GET: customerOrder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerOrder tbl_customerOrder = db.tbl_customerOrder.Find(id);
            if (tbl_customerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_customer = new SelectList(db.tbl_Customer, "CustomerID", "UserName", tbl_customerOrder.fk_id_customer);
            return View(tbl_customerOrder);
        }

        // POST: customerOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_customerOrder,fk_id_customer,name,phone,email")] tbl_customerOrder tbl_customerOrder)
        {
          
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(tbl_customerOrder).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message; 
                    }
                }

            }
            ViewBag.fk_id_customer = new SelectList(db.tbl_Customer, "CustomerID", "UserName", tbl_customerOrder.fk_id_customer);
            return View(tbl_customerOrder);
        }

        // GET: customerOrder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerOrder tbl_customerOrder = db.tbl_customerOrder.Find(id);
            if (tbl_customerOrder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_customerOrder);
        }

        // POST: customerOrder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            { try
                {
                    tbl_customerOrder tbl_customerOrder = db.tbl_customerOrder.Find(id);
                    db.tbl_customerOrder.Remove(tbl_customerOrder);
                    db.SaveChanges(); transaction.Commit();
                } 
                catch (Exception ex) 
                {
                    transaction.Rollback(); ViewData["Data"] = ex.Message; 
                }
            }
  
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
