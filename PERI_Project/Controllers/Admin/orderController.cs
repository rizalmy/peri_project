﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Admin
{
    public class orderController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: order
        public ActionResult Index()
        {
            var tbl_order = db.tbl_order.Include(t => t.tbl_customerOrder).Include(t => t.tbl_design).Include(t => t.tbl_product);
            return View(tbl_order.ToList());
        }

        // GET: order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_order tbl_order = db.tbl_order.Find(id);
            if (tbl_order == null)
            {
                return HttpNotFound();
            }
            return View(tbl_order);
        }

        // GET: order/Create
        public ActionResult Create()
        {
            tbl_customerOrder customerOrder = Session["Customer"] as tbl_customerOrder;
            tbl_order order = new tbl_order();
            order.fk_customer_id = customerOrder.pk_id_customerOrder;
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty");
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name");
            return View(order);

        }

        // POST: order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_order,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,price,date,order_status")] tbl_order order_table)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        order_table.order_status = "Not Yet";
                        db.tbl_order.Add(order_table);
                        order_table.date = DateTime.Now;
                        tbl_design design = db.tbl_design.Find(order_table.fk_design_id);
                        tbl_product product = db.tbl_product.Find(order_table.fk_product_id);
                        order_table.price = (order_table.lenght_meter * order_table.width_meter * product.product_price + design.design_price) * order_table.amount;
                        db.tbl_order.Add(order_table);
                        db.SaveChanges();
                        tbl_payment total = new tbl_payment();
                        total.fk_id_customerOrder = order_table.fk_customer_id;
                        double? totalprice = db.tbl_order.Where(item => item.fk_customer_id == total.fk_id_customerOrder).Select(item => item.price).DefaultIfEmpty().Sum();
                        total.order_status = "Not Yet";
                        total.total_price = totalprice;
                        total.payment_status = "Not Yet";
                        db.tbl_payment.Add(total);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index", "payment");

                    }
                    catch (Exception ex) { transaction.Rollback(); ViewData["Data"] = ex.Message; }
                }

            }
     
  

            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", order_table.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", order_table.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", order_table.fk_product_id);
            return View(order_table);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AnotherOrder([Bind(Include = "pk_id_order,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,price,date,order_status")] tbl_order order_table)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        order_table.date = DateTime.Now;
                        order_table.order_status = "Not Yet";
                        db.tbl_order.Add(order_table);
                        tbl_design design = db.tbl_design.Find(order_table.fk_design_id);
                        tbl_product product = db.tbl_product.Find(order_table.fk_product_id);
                        order_table.price = (order_table.lenght_meter * order_table.width_meter * product.product_price + design.design_price) * order_table.amount;
                        db.tbl_order.Add(order_table);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Create");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }

            }
            
            //ViewBag.fk_customer_id = new SelectList(db.customers, "pk_id_customer", "name", order_table.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", order_table.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", order_table.fk_product_id);
            return View(order_table);
        }




        // GET: order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_order tbl_order = db.tbl_order.Find(id);
            if (tbl_order == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_order.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", tbl_order.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", tbl_order.fk_product_id);
            return View(tbl_order);
        }

        // POST: order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(/*[Bind(Include = "pk_id_order,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,price,date,order_status,ImagePath")]*/ tbl_order tbl_order)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(tbl_order.ImageFile.FileName);
                        string extension = Path.GetExtension(tbl_order.ImageFile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        tbl_order.ImagePath = "~/Content/images/order_design/" /*+foldername+"/"*/+ fileName;
                        fileName = Path.Combine(Server.MapPath("~/Content/images/order_design/"), fileName);
                        tbl_order.ImageFile.SaveAs(fileName);
                        db.Entry(tbl_order).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        ModelState.Clear();
                        return RedirectToAction("Index","payment");

                    }
                    catch (Exception ex) { transaction.Rollback(); ViewData["Data"] = ex.Message; }
                }

            }
     
           
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", tbl_order.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", tbl_order.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", tbl_order.fk_product_id);
            return View(tbl_order);
        }

        [HttpGet]
        public ActionResult Upload(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_order order_table = db.tbl_order.Find(id);
            if (order_table == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", order_table.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", order_table.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", order_table.fk_product_id);
            return View(order_table);
        }

        [HttpPost]
        public ActionResult Upload(tbl_order imageModel)
        {
            //Directory.CreateDirectory(@"~/Image/" + foldername);
            string fileName = Path.GetFileNameWithoutExtension(imageModel.ImageFile.FileName);
            string extension = Path.GetExtension(imageModel.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            imageModel.ImagePath = "~/Content/images/order_design/" /*+foldername+"/"*/+ fileName;
            fileName = Path.Combine(Server.MapPath("~/Content/images/order_design/"), fileName);
            imageModel.ImageFile.SaveAs(fileName);
            db.Entry(imageModel).State = EntityState.Modified;
            db.SaveChanges();

            ModelState.Clear();
            return View();
        }

        [HttpGet]
        public ActionResult View(int id)
        {
            tbl_order imageModel = new tbl_order();
            imageModel = db.tbl_order.Where(x => x.pk_id_order == id).FirstOrDefault();
            return View(imageModel);
        }
        // GET: order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_order tbl_order = db.tbl_order.Find(id);
            if (tbl_order == null)
            {
                return HttpNotFound();
            }
            return View(tbl_order);
        }

        // POST: order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            { try
                {
                    tbl_order tbl_order = db.tbl_order.Find(id);
                    db.tbl_order.Remove(tbl_order);
                    db.SaveChanges(); transaction.Commit(); } catch (Exception ex) { transaction.Rollback(); ViewData["Data"] = ex.Message; } }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
