﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;
using System.IO;
namespace PERI_Project.Controllers.Owner
{
    public class productController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: product
        public ActionResult Index()
        {
            return View(db.tbl_product.ToList());
        }

        // GET: product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_product tbl_product = db.tbl_product.Find(id);
            if (tbl_product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_product);
        }

        // GET: product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(/*[Bind(Include = "pk_id_product,product_name,product_price")]*/ tbl_product tbl_product)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(tbl_product.ImageFile.FileName);
                        string extension = Path.GetExtension(tbl_product.ImageFile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        tbl_product.ImagePath = "~/Content/images/products/" /*+foldername+"/"*/+ fileName;
                        fileName = Path.Combine(Server.MapPath("~/Content/images/products/"), fileName);
                        tbl_product.ImageFile.SaveAs(fileName);
                        db.tbl_product.Add(tbl_product);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ViewData["error"] = ex.Message;
                    }
                }
            }
            return View(tbl_product);
        }

        // GET: product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_product tbl_product = db.tbl_product.Find(id);
            if (tbl_product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_product);
        }

        // POST: product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(/*[Bind(Include = "pk_id_product,product_name,product_price")]*/ tbl_product tbl_product)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(tbl_product.ImageFile.FileName);
                        string extension = Path.GetExtension(tbl_product.ImageFile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        tbl_product.ImagePath = "~/Content/images/products/" /*+foldername+"/"*/+ fileName;
                        fileName = Path.Combine(Server.MapPath("~/Content/images/products/"), fileName);
                        tbl_product.ImageFile.SaveAs(fileName);
                        db.Entry(tbl_product).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ViewData["error"] = ex.Message;
                    }
                }
            }
            return View(tbl_product);
        }

        // GET: product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_product tbl_product = db.tbl_product.Find(id);
            if (tbl_product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_product);
        }

        // POST: product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_product tbl_product = db.tbl_product.Find(id);
                    db.tbl_product.Remove(tbl_product);
                    db.SaveChanges();
                    transaction.Commit();
                    
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewData["error"] = ex.Message;
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
