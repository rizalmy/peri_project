﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class otherIncomeController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: otherIncome
        public ActionResult Index()
        {
            return View(db.tbl_otherIncome.ToList());
        }

        // GET: otherIncome/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_otherIncome tbl_otherIncome = db.tbl_otherIncome.Find(id);
            if (tbl_otherIncome == null)
            {
                return HttpNotFound();
            }
            return View(tbl_otherIncome);
        }

        // GET: otherIncome/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: otherIncome/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_otherIncome,incomeName,income,date")] tbl_otherIncome tbl_otherIncome)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tbl_otherIncome.Add(tbl_otherIncome);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }

            }
 
            return View(tbl_otherIncome);
        }

        // GET: otherIncome/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_otherIncome tbl_otherIncome = db.tbl_otherIncome.Find(id);
            if (tbl_otherIncome == null)
            {
                return HttpNotFound();
            }
            return View(tbl_otherIncome);
        }

        // POST: otherIncome/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_otherIncome,incomeName,income,date")] tbl_otherIncome tbl_otherIncome)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(tbl_otherIncome).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex) { transaction.Rollback(); ViewData["Data"] = ex.Message; }
                }

            }

 
            return View(tbl_otherIncome);
        }

        // GET: otherIncome/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_otherIncome tbl_otherIncome = db.tbl_otherIncome.Find(id);
            if (tbl_otherIncome == null)
            {
                return HttpNotFound();
            }
            return View(tbl_otherIncome);
        }

        // POST: otherIncome/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            { try
                {
                    tbl_otherIncome tbl_otherIncome = db.tbl_otherIncome.Find(id);
                    db.tbl_otherIncome.Remove(tbl_otherIncome);
                    db.SaveChanges(); 
                    transaction.Commit(); } 
                catch (Exception ex) 
                {
                    transaction.Rollback(); ViewData["Data"] = ex.Message; 
                } 
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
