﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class customerDebtController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: customerDebt
        public ActionResult Index()
        {
            var tbl_customerDebt = db.tbl_customerDebt.Include(t => t.tbl_payment);
            return View(tbl_customerDebt.ToList());
        }

        // GET: customerDebt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            if (tbl_customerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_customerDebt);
        }

        // GET: customerDebt/Create
        public ActionResult Create()
        {
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status");
            return View();
        }

        // POST: customerDebt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_cDebt,fk_id_payment,Debt,debt_status,payment")] tbl_customerDebt tbl_customerDebt)
        {
            if (ModelState.IsValid)
            {
                db.tbl_customerDebt.Add(tbl_customerDebt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_customerDebt.fk_id_payment);
            return View(tbl_customerDebt);
        }

        // GET: customerDebt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            if (tbl_customerDebt == null)
            {
                return HttpNotFound();
            }
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_customerDebt.fk_id_payment);
            return View(tbl_customerDebt);
        }

        // POST: customerDebt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_cDebt,fk_id_payment,Debt,debt_status,payment")] tbl_customerDebt tbl_customerDebt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_customerDebt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fk_id_payment = new SelectList(db.tbl_payment, "pk_id_payment", "payment_status", tbl_customerDebt.fk_id_payment);
            return View(tbl_customerDebt);
        }

        // GET: customerDebt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            if (tbl_customerDebt == null)
            {
                return HttpNotFound();
            }
            return View(tbl_customerDebt);
        }

        // POST: customerDebt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_customerDebt tbl_customerDebt = db.tbl_customerDebt.Find(id);
            db.tbl_customerDebt.Remove(tbl_customerDebt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
