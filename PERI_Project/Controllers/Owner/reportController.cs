﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PERI_Project.Controllers.Owner
{
    public class reportController : Controller
    {
        // GET: report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult employee()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-slkoj7v/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/reportEmployee";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();

        }
    }
}