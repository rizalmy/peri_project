﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers.Owner
{
    public class spendingController : Controller
    {
        private PERIEntityDB db = new PERIEntityDB();

        // GET: spending
        public ActionResult Index()
        {
            return View(db.tbl_spending.ToList());
        }

        // GET: spending/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            if (tbl_spending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spending);
        }

        // GET: spending/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: spending/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_spending,spending_name,spending_cash,payment,date")] tbl_spending tbl_spending)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tbl_spending.Add(tbl_spending);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ViewData["error"] = ex.Message;
                    }
                }
            }    
            return View(tbl_spending);
        }

        // GET: spending/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            if (tbl_spending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spending);
        }

        // POST: spending/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_spending,spending_name,spending_cash,payment,date")] tbl_spending tbl_spending)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(tbl_spending).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ViewData["error"] = ex.Message;
                    }
                }
            }
            return View(tbl_spending);
        }

        // GET: spending/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_spending tbl_spending = db.tbl_spending.Find(id);
            if (tbl_spending == null)
            {
                return HttpNotFound();
            }
            return View(tbl_spending);
        }

        // POST: spending/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_spending tbl_spending = db.tbl_spending.Find(id);
                    db.tbl_spending.Remove(tbl_spending);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewData["error"] = ex.Message;
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
