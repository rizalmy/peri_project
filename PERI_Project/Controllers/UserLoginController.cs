﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers
{
    public class UserLoginController : Controller
    {
        PERIEntityDB db = new PERIEntityDB();
        // GET: UserLogin
        public ActionResult LoginPage()
        {
            return View();
        }

        // Login POST
        [HttpPost]
        public ActionResult LoginPage(tbl_Customer item)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {

                var data = dc.tbl_Customer.Where(x => x.Email.Equals(item.Email)).FirstOrDefault();
                var data1 = dc.tbl_customerOrder.Where(x => x.email.Equals(item.Email)).FirstOrDefault();
                if (data != null)
                {
                    if (data.IsActive == false)
                    {
                        TempData["Message"] = "Please verify your email first";
                        TempData.Keep();
                        return RedirectToAction("LoginPage", "UserLogin");
                    }
                    else
                    {
                        if (string.Compare(Crypto.Hash(item.Password), data.Password) == 0)
                        {
                            Session["CustID"] = data.CustomerID;
                            Session["CustUserName"] = data.UserName;
                            Session["CustEmail"] = data.Email;
                            Session["CustName"] = data.Name;
                            Session["CustIDOrder"] = data1.pk_id_customerOrder;
                            return RedirectToAction("HomePage", "HomeCustomer");
                           
                        }
                        else
                        {
                            TempData["Message"] = "Your Password Wrong";
                            TempData.Keep();
                            return RedirectToAction("LoginPage", "UserLogin");
                        }
                    }
                }
                else
                {
                    TempData["Message"] = "Your Email Wrong";
                    TempData.Keep();
                    return RedirectToAction("LoginPage", "UserLogin");
                }
            }
        }

        // register action
        [HttpGet]
        public ActionResult Register(int id = 0)
        {
            tbl_Customer cust = new tbl_Customer();
            return View(cust);
        }

        // register POST action
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Exclude = "IsActive,ActivationCode")] tbl_Customer customer)
        {
            bool Status = false;
            string message = "";

            // model validation
            if (ModelState.IsValid)
            {
                #region // email already exist
                var isExist = isEmailExist(customer.Email);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist");
                }
                #endregion

                #region // generate activation code
                customer.ActivationCode = Guid.NewGuid();
                #endregion

                #region // password hasing
                customer.Password = Crypto.Hash(customer.Password);
                customer.ConfirmPassword = Crypto.Hash(customer.ConfirmPassword);
                #endregion
                customer.IsActive = false;

                #region // add upload ktp
                string filename = Path.GetFileNameWithoutExtension(customer.ImageFile.FileName);
                string extension = Path.GetExtension(customer.ImageFile.FileName);
                filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                customer.ImagePath = "~/Content/images/KTP/" + filename;
                filename = Path.Combine(Server.MapPath("~/Content/images/KTP/"), filename);
                customer.ImageFile.SaveAs(filename);
                #endregion

                #region // save data to database
                using (PERIEntityDB dc = new PERIEntityDB())
                {
                    dc.tbl_Customer.Add(customer);
                    dc.SaveChanges();
                    
                    if (customer.CustomerID > 0)
                    {
                        tbl_customerOrder custOrder = new tbl_customerOrder();
                        custOrder.fk_id_customer = customer.CustomerID;
                        custOrder.name = customer.Name;
                        custOrder.phone = customer.Phone;
                        custOrder.email = customer.Email;
                        db.tbl_customerOrder.Add(custOrder);
                        db.SaveChanges();
                    }

                    // send email to customer
                    SendVerivicationLinkEmail(customer.Email, customer.ActivationCode.ToString());
                    message = "Registration successfully done, Account activation link " +
                        "has been sent to your email:" + customer.Email;
                    Status = true;
                }
                #endregion
            }
            else
            {
                message = "Invailid request";
            }

            ViewBag.Message = message;
            ViewBag.Status = Status;
            ModelState.Clear();
            return View(customer);
        }

        // Verify email 
        [HttpGet]
        public ActionResult VerifyAcount(string id)
        {
            bool Status = false;
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                dc.Configuration.ValidateOnSaveEnabled = false;

                var v = dc.tbl_Customer.Where(x => x.ActivationCode == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.IsActive = true;
                    dc.SaveChanges();
                    Status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request !";
                }
            }
            ViewBag.Status = Status;
            return View();
        }

        // Logout
        public ActionResult Logout()
        {
            int userId = (int)Session["CustID"];
            int orderId = (int)Session["CustIDOrder"];
            Session.Abandon();
            return RedirectToAction("HomePage", "HomeCustomer");
        }

        [NonAction]
        public bool isEmailExist(string email)
        {
            using (PERIEntityDB dc = new PERIEntityDB())
            {
                var v = dc.tbl_Customer.Where(x => x.Email == email).FirstOrDefault();
                return v != null;
            }
        }

        [NonAction]
        public void SendVerivicationLinkEmail(string email, string activationcode)
        {
            var verifyUrl = "/UserLogin/VerifyAcount/" + activationcode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("periofficial.company@gmail.com", "Your Awesome");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "logPeri987"; // Replace with actual password
            string subject = "Your account is sucessfuly created";

            string body = "<br/><br/>We are excited to tell you that PERI Awesome acoount is" +
                " successfully created. Please click on the below link to verify your account" +
                " <b/><b/><a href='" + link + "'>" + link + "</a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var msg = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true

            })
                smtp.Send(msg);

        }
    }
}