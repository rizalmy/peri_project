﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PERI_Project.Models;

namespace PERI_Project.Controllers
{
    public class HomeCustomerController : Controller
    {
        PERIEntityDB db = new PERIEntityDB();
        // GET: HomeCustomer
        public ActionResult HomePage()
        {
            return View();
        }
        
        public ActionResult Order()
        {
            int idCust = (int)Session["CustID"];
            int idOrder = (int)Session["CustIDOrder"];
            tbl_customerOrder custOrder = db.tbl_customerOrder.Where(x => x.pk_id_customerOrder == idOrder && x.fk_id_customer == idCust).FirstOrDefault();
            tbl_order order = new tbl_order();
            order.fk_customer_id = custOrder.pk_id_customerOrder;
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty");
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name");
            return View(order);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Order([Bind(Include = "pk_id_order,fk_customer_id,details_order,lenght_meter,width_meter,fk_design_id,fk_product_id,amount,date")] tbl_order order_table)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
         
                        order_table.order_status = "Not Yet";
                        db.tbl_order.Add(order_table);
                        order_table.date = DateTime.Now;
                        tbl_design design = db.tbl_design.Find(order_table.fk_design_id);
                        tbl_product product = db.tbl_product.Find(order_table.fk_product_id);
                        order_table.price = (order_table.lenght_meter * order_table.width_meter * product.product_price + design.design_price) * order_table.amount;
                        
                        /*
                        #region // add upload design
                        string filename = Path.GetFileNameWithoutExtension(order_table.ImageFile.FileName);
                        string extension = Path.GetExtension(order_table.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                        order_table.ImagePath = "~/Content/images/order_design/" + filename;
                        filename = Path.Combine(Server.MapPath("~/Content/images/order_design/"), filename);
                        order_table.ImageFile.SaveAs(filename);
                        #endregion*/

                        db.tbl_order.Add(order_table);
                        db.SaveChanges();
                        tbl_payment total = new tbl_payment();
                        total.fk_id_customerOrder = order_table.fk_customer_id;
                        double? totalprice = db.tbl_order.Where(item => item.fk_customer_id == total.fk_id_customerOrder).Select(item => item.price).DefaultIfEmpty().Sum();
                        total.order_status = "Not Yet";
                        total.total_price = totalprice;
                        total.payment_status = "Not Yet";
                        db.tbl_payment.Add(total);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Payment","HomeCustomer");

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback(); ViewData["Data"] = ex.Message;
                    }
                }
            }
            ViewBag.fk_customer_id = new SelectList(db.tbl_customerOrder, "pk_id_customerOrder", "name", order_table.fk_customer_id);
            ViewBag.fk_design_id = new SelectList(db.tbl_design, "pk_id_design", "difficulty", order_table.fk_design_id);
            ViewBag.fk_product_id = new SelectList(db.tbl_product, "pk_id_product", "product_name", order_table.fk_product_id);
            return View(order_table);
        }

        public ActionResult Payment()
        {
            int idOrder = (int)Session["CustIDOrder"];
            tbl_customerOrder customerOrder = new tbl_customerOrder();
            var tbl_payment = db.tbl_payment.Where(x => x.fk_id_customerOrder == idOrder);
            return View(tbl_payment.ToList());
        }
        
        public ActionResult Payment_Proof(int id)
        {
            tbl_payment payment = db.tbl_payment.Where(x => x.pk_id_payment == id).FirstOrDefault();
            return View(payment);
        }
        
    }
}